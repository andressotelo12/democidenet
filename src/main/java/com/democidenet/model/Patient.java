package com.democidenet.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Digits;

/**
 * @Entity PATIENT Model for save data in db of patients,
 * have instrate de  validations en the scope @validate
 * @author jaime Sotelo
 **/

@Entity
@Table(name = "PATIENT")

public class Patient {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Patient_ID")
	Long patientID;
	@Column(name="ID")
	@Digits(integer=20, fraction=0, message="Number invalid" )
	@Size(min=0,max=20, message 
		      = "The max value for id is 20 Characters")
	String id;
	@Column(name="First_name")
	@Size(min=0,max=225, message 
    = "The max value for First name is 255 Characters")
	String firstname;
	@Column(name="Last_name")
	@Size(min=0,max=225, message 
    = "The max value for Last Name is 255 Characters")
	String lastname;
	@Column(name="Email")
	@Size(min=0,max=225, message 
    = "The max value for Email is 255 Characters")
	@Email(message="Please provide a valid email address")
	@Pattern(regexp=".+@.+\\..+", message="Please provide a valid email address")
	String email;
	@Column(name="Phone")
	@Size(min=0,max=20, message 
    = "The max value for Phone is 20 Characters")
	String phone;
	

	public Patient(){
		   super();
		}
	
	public Patient(Long patientID,
			@Digits(integer = 20, fraction = 0, message = "Number invalid") @Size(min = 0, max = 20, message = "The max value for id is 20 Characters") String id,
			@Size(min = 0, max = 225, message = "The max value for First name is 255 Characters") String firstname,
			@Size(min = 0, max = 225, message = "The max value for Last Name is 255 Characters") String lastname,
			@Size(min = 0, max = 225, message = "The max value for Email is 255 Characters") @Email(message = "Please provide a valid email address") @Pattern(regexp = ".+@.+\\..+", message = "Please provide a valid email address") String email,
			@Size(min = 0, max = 20, message = "The max value for Phone is 20 Characters") String phone) {
		
		
		this.patientID = patientID;
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.phone = phone;
		
	}

	public String getId() {
		return id;
	}
	
	public Long getPatientID() {
		return patientID;
	}
	public void setPatientID(Long patientID) {
		this.patientID = patientID;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public void setId(String id) {
		this.id = id;
	}	

}
