package com.democidenet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoCidenetApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoCidenetApplication.class, args);
	}

}
