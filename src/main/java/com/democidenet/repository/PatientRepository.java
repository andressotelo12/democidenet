package com.democidenet.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.democidenet.model.Patient;

/**
 * @Repository PatientRepository Interface for manage de Jpa hibernate for patients
 * 
 * @author jaime Sotelo
 **/

@Repository
public interface PatientRepository  extends JpaRepository<Patient, Long> {
	
	/*@Query for manage the search by id in the db
	 * */
	  @Query("SELECT p FROM Patient p where p.id = ?1 ") List<Patient>
	  findByIdNumber(String id);

}
