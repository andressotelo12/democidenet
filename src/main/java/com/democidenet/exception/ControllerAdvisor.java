package com.democidenet.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDate;
import java.time.LocalDateTime;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
*
* Controller Manage the Global exceptions in the App
* @author jaime Sotelo
**/


@ControllerAdvice

public class ControllerAdvisor extends ResponseEntityExceptionHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(ControllerAdvisor.class);

	/*
	 * @ExceptionHandler : PatientFoundException For manage exceptions when the patient exists
	 * @param PatientFoundException : Exeption
	 * @param WebRequest :  Request Service
	 * 
	 * */
    @ExceptionHandler(PatientFoundException.class)
    public ResponseEntity<Object> handlePatientFoundException(
        PatientFoundException ex, WebRequest request) {
    	
    	logger.info("ControllerAdvisor.handlePatientFoundException");
    	
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }
    
    /*
	 * @ExceptionHandler : PatientValidException For manage validation in Hibernate
	 * @param PatientValidException : Exception
	 * @param WebRequest :  Request Service
	 * 
	 * */
    
    @ExceptionHandler(PatientValidException.class)
    public ResponseEntity<Object> handlePatientValidExceptionn(
    		PatientValidException ex, WebRequest request) {
    	
    	logger.info("ControllerAdvisor.handlePatientValidException");

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", ex.getMessage());

        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  
    /*
	 * @ExceptionHandler : handleMethodArgumentNotValid oher Arguments invalid
	 * @param MethodArgumentNotValidException : Exception
	 * @param WebRequest :  Request Service
	 * 
	 * */
	
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException ex, HttpHeaders headers, 
        HttpStatus status, WebRequest request) {
    	
    	logger.info("ControllerAdvisor.handleMethodArgumentNotValid");

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDate.now());
        body.put("status", status.value());

        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        body.put("errors", errors);

        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }
}