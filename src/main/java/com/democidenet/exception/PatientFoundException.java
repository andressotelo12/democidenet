package com.democidenet.exception;

/**
 * Validate Exception for existing Patients
 * @author jaime Sotelo
 **/

public class PatientFoundException  extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	/*
	 * @PatientFoundException : Return the message went the patient exist
	 * @param id : number id patient
	 * 
	 **/	

	public PatientFoundException(String id) {
		
        super("Patient with Id "+id+" Exist");
    }

}
