package com.democidenet.exception;

import org.hibernate.HibernateException;

/**
 * Validate Exception for save Patients
 * @author jaime Sotelo
 **/	

public class PatientValidException extends HibernateException {
	
	/*
	 * @PatientValidException : Return the message went exist error in the validate
	 * @param message : Message of the validate
	 * 
	 **/

	private static final long serialVersionUID = 1L;    
	
	public PatientValidException(String message) {		
		// TODO Auto-generated constructor stub
		super(message);
	}
	

}
