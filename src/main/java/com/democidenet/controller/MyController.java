package com.democidenet.controller;

import com.democidenet.model.Patient;
import com.democidenet.service.PatientService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
*
* Controller for get the patient and save the patient rest service
*
*
* @author jaime Sotelo
*/

@RestController

public class MyController {
	
	private static final Logger logger = LoggerFactory.getLogger(MyController.class);
	
	@Autowired
    private PatientService patientService;
	
	/*
	 * @RequestMapping path for add Patient for add new Patient
	 * @param method = post
	 * @param produces = produce a response format Json.
	 * */

     
    @RequestMapping(value = "/patient/add", method = RequestMethod.POST, produces = "application/json") 
     public Patient addPatient(Patient patient) { 
    	
    	logger.info("MyController.addPatient");
    	
        return patientService.save(patient);
    }

}
