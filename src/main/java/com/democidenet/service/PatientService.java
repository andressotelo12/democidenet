package com.democidenet.service;

import com.democidenet.model.Patient;

/**
*
* Interface for expose service  PatientService
* @author jaime Sotelo
**/

public interface PatientService {
	
	public Patient findByIdNumber(String id);
	
	public Patient save(Patient patient);

}
