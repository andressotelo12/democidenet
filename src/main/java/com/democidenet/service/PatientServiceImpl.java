package com.democidenet.service;

import java.util.List;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.democidenet.exception.PatientFoundException;
import com.democidenet.exception.PatientValidException;
import com.democidenet.model.Patient;
import com.democidenet.repository.PatientRepository;


/**
* @Service PatientServiceImpl  for implement the service  PatientService
* @author jaime Sotelo
**/

@Service

public class PatientServiceImpl implements PatientService {

	@Autowired
	private PatientRepository patientRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(PatientServiceImpl.class);
	
	
	/**
	 * @param id Id for find patient in the database
	 **/
	@Override
	public Patient findByIdNumber(String id) {
		
		logger.info("PatientServiceImpl.findByIdNumber");

		List<Patient> a = patientRepository.findByIdNumber(id);
		Patient patien = a.get(0);
		return patien;
	}
	
	/**
	 * @param Patient Patient accept to save in db
	 **/

	@Override
	public Patient save(Patient patientNew) {

		logger.info("PatientServiceImpl.save");
		
		String id = patientNew.getId().toString();
		List<Patient> a = patientRepository.findByIdNumber(id);

		if (a.size() > 0) {
			throw new PatientFoundException(id);
		} else {
			
			ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
			javax.validation.Validator validator = factory.getValidator();
			Set<ConstraintViolation<Patient>> violations = validator.validate(patientNew);
			
			if(violations.size()>0) {
				for (ConstraintViolation<Patient> violation : violations) {			    
				    throw new PatientValidException(violation.getMessage());
				}				
			}else {
				patientNew = patientRepository.save(patientNew); 				
			}
		}

		return patientNew;
		
	}

}
